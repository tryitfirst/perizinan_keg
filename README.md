NAMA SISTEM : SISTEM PERIZINAN KEGIATAN

PENJELASAN SISTEM : Sistem ini akan mengangani masalah perizinan kegiatan, dengan alur yang sama seperti perizinan seperti biasanya. 
Keunggulannya, user dapat mengunggah proposal  sewaktu waktu, Pak Onggo, Pak Dwi dan Bu Nanik juga dapat memeriksa dan meng-acc proposal kegiatan sewaktu waktu. 
Sehingga pengajua proposal tidak perlu lagi menunggu kesenggangan waktu dua belah pihak untuk saling bertatap muka dan juga lebih menghemat waktu dan kertas.
Mahasiswa mengajukan proposal, pak Onggo memeriksa dan apabila di-acc akan diteruskan ke pak Dwi (jika menggunakan dana jurusan) kemudian jika telah disetujui akan 

diteruskan ke bu Nanik,  
dan jika tidak menggunakan dana jurusan maka proposal yang sudah di-acc oleh pak Onggo akan langsung di teruskan ke bu Nanik. Di page user mahasiswa yang submitting 

proposal akan terlihat status ACC proposalnya

PENGEMBANG : Tanah Liat 
Bagas Andita Setiyawan - 5113100029 (Project Manager)
Umy Chasanah Noor Rizqi - 5113100082

JOBDESK :
- Rancangan sistem 	: Bagas, Umy
- Login & Register 	: Bagas
- Upload & display file : Bagas
- Timeline (Calendar)	: Bagas
- CSS			: Bagas
- User Privillege  	: Umy
- ACC			: Umy
- Status Proposal	: Umy


INSTALASI :
-4 tipe user : Mahasiswa, Pak onggo(raditiyo@gmail.com,onggo), Pak Dwi(dwis@gmail.com,pakdwi), Bu Nanik(naniks@gmail.com,bunanik)
user-non-login hanya dapat melihat timeline.
Mahasiswa dapat men-submit proposal, melihat status ACC proposalnya, dan juga melihat timeline
Pak onggo dapat melihat proposal yang hanya belum di ACC oleh siapapun. Kemudian pak onggo dapat memberikan ACC/tolak terhadap proposal kegiatan. dan juga dapat 

melihat timeline
Pak Dwi  hanya dapat melihat dan melakukan acc/tolak proposal yang menggunakan dana jurusan yang telah disetujui pak Onggo
Bu Nanik hanya dapat melihat dan melakukan acc/tolak proposal yang telah disetujui pak onggo saja (tidak menggunakan dana jurusan) dan yang telah disetujui pak onggo 

dan pak dwi (menggunakan dana jurusan) serta dapat melihat timeline

LANGKAH 
-Mahasiswa yang belum memiliki akun, mendaftar terebih dahulu. kemudian Login
-Mahasiswa mengupload proposal kegiatannya(hanya pdf).
-Pak onggo, Pak dwi(jika menggunakan dana jurusan), bu nanik melakukan ACC/tolak. dan asing masing dari mereka dapat memerikan catatan terhadap proposal tersebut saat 

akan melakukan ACC/tolak
-Mahasiswa yang mengupload proposal yang bersangkutan hanya dapat melihat status proposal yang dia submit. Misal status -> proposal telah disetujui pak onggo, proposal 

ditolak dll. dan juga terdapat kolom catatan dari ketiga dosen
-Apabila status "Proposal telah di setujui bu Nanik" Maka proposal telah disetujui oleh jurusan dan dapat di realisasikan
dan apabila status "proposal ditolak" maka kegiatan ditolak dan tidak dapat direalisasikan